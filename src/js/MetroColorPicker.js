var MetroColorPicker = new Class({

	initialize: function(el) {
		this.input = el;

		this.build();
	},

	build: function() {
		this.input.setStyle('display', 'none');
		this.input.get('value') ? this.input.set('value', this.input.get('value')) : null;

		this.button = new Element('div', {
			class: 'mcp-button',
			style: "cursor: pointer; width: 32px; height: 32px; box-shadow: 1px 1px 3px #000; margin: 0; margin-right: 10px; display: inline-block; background-color: " + this.input.get('value') + "; background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkE1MkNCQjgzMjY2ODExRTVBQ0RDODU2ODQzODIzQjRCIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkE1MkNCQjg0MjY2ODExRTVBQ0RDODU2ODQzODIzQjRCIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6QTUyQ0JCODEyNjY4MTFFNUFDREM4NTY4NDM4MjNCNEIiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6QTUyQ0JCODIyNjY4MTFFNUFDREM4NTY4NDM4MjNCNEIiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6kteCyAAAAhElEQVR42uyX0QnAIAxEk+JEupPu5E7OZFu/TrCfJgXvQFB/fHcGMdofiaMucVaAuRqf3X+RAAEIQAACEIAABCBA2PXTOTqBoVKKtNamvRij1FptEsg5L6HMriClNAa6x7VJDWAKK/dbawBTeJuvlfvtAF+1gFLoDfX4d8ClS3ZP4BZgALDeH2qP+Sn3AAAAAElFTkSuQmCC')",
		});

		this.button.inject(this.input, 'before');
		this.button.addEvent('click', this.openPicker.bind(this));

		this.shade = new Element('div', { 'class': 'mcp-shade' });
		this.modal = new Element('div', { 'class': 'mcp-modal', 'html': '<header class="mcp-modal-header"><h3>Choose a Color</h3><i>X</i></header><section class="mcp-modal-body"><div class="mcp-tile-container"><div class="mcp-color-tile">None</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#a4c400">Lime</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#60a917">Green</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#008a00">Emerald</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#00aba9">Teal</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#00aff0">Blue</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#1ba1e2">Cyan</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#0050ef">Cobalt</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#6a00ff">Indigo</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#a0f">Violet</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#dc4fad">Pink</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#d80073">Magenta</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#a20025">Crimson</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#ce352c">Red</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#fa6800">Orange</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#f0a30a">Amber</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#e3c800">Yellow</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#825a2c">Brown</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#6d8764">Olive</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#647687">Steel</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#76608a">Mauve</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#87794e">Taupe</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#555">Gray</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#333">Dark</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#222">Darker</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#63362f">Dark Brown</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#640024">Dark Crimson</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#81003c">Dark Magenta</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#4b0096">Dark Indigo</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#1b6eae">Dark Cyan</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#00356a">Dark Cobalt</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#004050">Dark Teal</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#003e00">Dark Emerald</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#128023">Dark Green</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#bf5a15">Dark Orange</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#9a1616">Dark Red</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#9a165a">Dark Pink</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#57169a">Dark Violet</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#16499a">Dark Blue</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#4390df">Light Blue</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#da5a53">Light Red</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#7ad61d">Light Green</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#0cf">Lighter Blue</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#45fffd">Light Teal</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#78aa1c">Light Olive</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#c29008">Light Orange</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#f472d0">Light Pink</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#999">Gray Light</div></div><div class="mcp-tile-container"><div class="mcp-color-tile" style="background-color:#eee">Gray Lighter</div></div></section>' });
	},

	openPicker: function(e) {
		this.shade ? this.shade.inject(document.body) : null;
		this.modal ? this.modal.inject(document.body) : null;
		this.modal.getElement('i').addEvent('click', this.closePicker.bind(this));;
		this.modal.getElements('.mcp-color-tile').addEvent('click', this.selectColor.bind(this));
	},

	closePicker: function(e) {
		this.shade.addClass('mcp-closed');
		this.modal.addClass('mcp-closed');
		(function() {
			var tiles = this.modal.getElements('.mcp-color-tile');
			this.modal.getElement('i').removeEvents();
			tiles.removeEvents();
			tiles.removeClass('mcp-selected');
			tiles.removeClass('mcp-disabled');
			this.shade.removeClass('mcp-closed');
			this.modal.removeClass('mcp-closed');
			this.shade.getParent().removeChild(this.shade);
			this.modal.getParent().removeChild(this.modal);
		}).delay(500, this);
	},

	loadPartial: function(name) {
		name = name ? name.toLowerCase() : '';
		new Request.HTML({
			url: 'partials/' + name + '.html',
			onSuccess: function(response) {
				this[name] = response[0];
			}.bind(this)
		}).send();
	},

	selectColor: function(e) {
		var tiles = this.modal.getElements('.mcp-color-tile');
		var color = e.target.getStyle('background-color');
		console.log(color);
		tiles.removeEvents();
		tiles.addClass('mcp-disabled');
		e.target.removeClass('mcp-disabled');
		e.target.addClass('mcp-selected');
		this.button.setStyle('background-color', color);
		this.input.set('value', color);
		this.closePicker.delay(1000, this);
	}

});

document.addEvent('domready', function() {
	var els = document.getElements('input.metro-color-picker');
	for (var i = 0; i < els.length; ++i) {
		new MetroColorPicker(els[i]);
	}
});