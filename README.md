Metro Color Picker
===

To download and use the Metro Color Picker, just [download the newest version](https://bitbucket.org/stevie-mccomb/metro-color-picker/downloads/MetroColorPicker.min.js) of the color picker from the [Downloads Section](https://bitbucket.org/stevie-mccomb/metro-color-picker/downloads) and include it in your project like any other JS file. Feel free to download any/all of the files in the `/src` directory and make changes to create your own version of the Metro Color Picker.